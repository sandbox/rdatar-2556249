README.txt
==========

Better Content Picker
A module providing an content search auxiliary to /admin/content.
This module is very useful for sites with many content types which in turn
have large number of fields With this module you will be able to search for
any piece of content based on values in any field of a content type.
Certain fields like 'nlmfield_contributor', 'taxonomy_term_reference',
'list_boolean', 'list_text', 'image', 'widget' are not searched.

INSTRUCTIONS
======================
1. Download and enable the module from module via /admin/modules or
through drush command: drush en better_content_picker.
2. Click on configure link from /admin/modules or from /admin/config to
choose content types to be added in search.
3. Once the module is enable click on a menu in admin menu
Better Content Picker
Menu link: /admin/settings/better_content_picker
4. Choose the node status from the drop down (default is published).
5. Enter title if you would like to search using title and click submit.
-- OR --
6. Choose the content type (all of its the fields will be displayed).
7. Enter the value in the field which you want to search.
8. Click on Submit.
9. All results will be displayed at the bottom of the page.
10. You can edit, delete or check field values in devel module (if enabled)


AUTHOR/MAINTAINER
======================
-rajashree datar <rdatar at faichi DOT com>
======================
